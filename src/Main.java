public class Main {

    private static final int Rows = 5;
    private static final int[][] fixedNumberCodes = {
            {1, 4, 0, 4, 1},
            {0, 3, 0, 3, 0},
            {1, 3, 1, 2, 1},
            {1, 3, 1, 3, 1},
            {0, 4, 1, 3, 0},
            {1, 2, 1, 3, 1},
            {1, 2, 1, 4, 1},
            {1, 3, 0, 3, 0},
            {1, 4, 1, 4, 1},
            {1, 4, 1, 3, 1}
    };
    private String[] fixedRows = new String[Rows];

    private int[][] inputtedNumberCodes;
    private int size = 1;
    private int[] numbers;


    private Main(int size, int[] numbers) {
        if (size > 1) {
            this.size = size;
        }
        this.numbers = numbers;
        inputtedNumberCodes = new int[numbers.length][Rows];
        calculateFixedRows();
        getInputtedNumberCodes();
    }

    private void calculateFixedRows() {
        String empty = " ", dash = "-", left_i = "|", right_i = "|";
        for (int i = 0; i < size - 1; i++) {
            empty += " ";
            dash += "-";
            left_i += " ";
            right_i = " " + right_i;
        }
        fixedRows[0] = empty + empty + empty;
        fixedRows[1] = empty + dash + empty;
        fixedRows[2] = left_i + empty + empty;
        fixedRows[3] = empty + empty + right_i;
        fixedRows[4] = left_i + empty + right_i;
    }

    private void getInputtedNumberCodes() {
        for (int i = 0; i < numbers.length; i++) {
            inputtedNumberCodes[i] = fixedNumberCodes[numbers[i]];
        }
    }

    private void printResultRowByRow() {
        int times = 0;
        for (int i = 0; i < 5; i++) {

            for (int j = 0; j < numbers.length; j++) {
                printRowByCode(inputtedNumberCodes[j][i]);
            }

            if (i % 2 != 0 && times < size - 1) {
                i--;
                times++;
            } else {
                times = 0;
            }

            printRowByCode(-1);
        }
    }

    private void printRowByCode(int code) {
        if (code < 0) {
            System.out.println();
        } else {
            System.out.print(fixedRows[code]);
        }
    }

    public static void main(String[] args) {
        Main main = new Main(3, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
        main.printResultRowByRow();
    }
}
